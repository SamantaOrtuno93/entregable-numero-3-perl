use strict;
use warnings;

sub primera_variacion (\@$) {
	my($lista, $longitud) = @_;

	my @primera = ();

	foreach (1..$longitud) {
		push @primera, $$lista[0];
	}
	
	return @primera;
}

sub siguiente_variacion (\@\@) {    
    my ($starting, $current) = @_;
	
	# Construye una tabla que asocia los valores de $starting con su índice en el array	
    my %h;
    @h{@$starting} = 0 .. scalar @$starting - 1;
    
	# Indices de la variacion actual
	my @actual = @h{@$current};
	
	my $longitud = scalar @actual;
	
	# Calcula la siguiente variación
		
	# Intenta incrementar la actual
	my $incremento = 0;	# falso
	my $i = 0;
	while ($i < $longitud and not $incremento) {
			
		if ($actual[$i] == scalar(@$starting) - 1) {
			$actual[$i] = 0;
		}
		else {
			$actual[$i]++;
			$incremento = 1; # verdadero
		}
		$i++;
	}

	if ($incremento) {
		return @$starting[@actual];
	}
	else {
		return ();
	}	
}

sub permutar (\@\@) {    
    my ($starting, $current) = @_;
 
    # Construye una tabla que asocia los valores de $starting con su índice en el array
     
    my %h;
    @h{@$starting} = 0 .. scalar @$starting - 1;
     
    # Indices de la permutación actual
    my @idx = @h{@$current};
     
    # Busca el índice de la siguiente permutación
    my $p = scalar @idx - 1;
    --$p while $idx[$p-1] > $idx[$p];
     
    if ($p == 0) { # es igual $starting
        return ();
    }   
     
    # En @idx genera los índices de la siguiente permutación
     
    my $q = $p;
    push @idx, reverse splice @idx, $p;
    ++$q while $idx[$p-1] > $idx[$q];
    @idx[$p-1,$q] = @idx[$q,$p-1];
         
    return @$starting[@idx];    
}


1;
