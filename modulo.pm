use lib ('.');
use variaciones;

#Parte 1
sub fusionar {my($cadena1, $cadena2) = @_;

my $longitud1 = length($cadena1);
my $longitud2 = length($cadena2);
$resultado{"cadena1"} = $cadena1;
$resultado{"cadena2"} = $cadena2;
$resultado{"fusion"} = "";

#Hago una lista con pedazos de tamaño 1 de la cadena 1

@lista_cadena1 = ();
$nueva = "";
foreach $i (0..$longitud1-1) {
	$caracter = chop($cadena1);
	$nueva = $caracter.$nueva;
	push (@lista_cadena1, $nueva);
}

#Hago una lista con pedazos de tamaño 1 de la cadena 2

$cadena2 = reverse $cadena2;
@lista_cadena2 = ();
$nueva2="";
foreach $x (0..$longitud2-1) {
	$caracter2 = chop($cadena2);
	$nueva2 = $nueva2.$caracter2;
	push (@lista_cadena2, $nueva2);
}

#Comparo los elementos de ambas listas para ver si son iguales, 
#consiguiendo el patrón que los une

$resultado{"solapamiento"} = 0;
foreach $m (@lista_cadena1) {
	foreach $c (@lista_cadena2) {
		if ($m eq $c) {
		$patron = $m;
		$resultado{"solapamiento"} = length($c);	
		}
		
	}
}

#Si existe solapamiento elimino los carácteres del final de la cadena 1
#que la he renombrado anteriormente como $nueva.

if (exists $resultado{"solapamiento"}) {
foreach $g (0..$resultado{"solapamiento"}-1) {
	chop($nueva);
} 
}

#Después de eliminar el patrón, fusiono ambas cadenas y retorno el 
#diccionario con toda la información.

my $resultado = $nueva.$nueva2;
$resultado{"fusion"} = $resultado;
return %resultado;


}


sub fusionar_lista {@inicio = @_;


$len = scalar(@inicio)-1;

#En este caso, en el primer paso, debo fusionar las dos primeras y
#a posteriori, la fusión que he obtenido anteriormente con el siguiente
#del array. 

foreach $l (0..$len){
	if ($l == 0) {
	%resultado = fusionar($inicio[$l], $inicio[$l+1]);
	$anterior = $resultado{'fusion'};
} else {
	%resultado = fusionar($anterior, $inicio[$l+1]);
	$anterior = $resultado{'fusion'};

}

}

return $resultado{'fusion'}
}



#Parte 2




sub  ensamblar_busqueda_exhaustiva{@secuencias = @_;

#Pongo un soa muy alto
	
$soa = 100000;


#permuto la lista y para cada permutacion, fusiono la lista obtenida


my @actual = @secuencias;
while (@actual) { 
	@actual = permutar @secuencias, @actual;
	foreach $i (@actual) {
		$resultado = fusionar_lista(@actual);
		#print $resultado, length($resultado), "\n";
	}
	#Después veo si la longitud es la más pequeña, me quedo con ese
	#resultado.
	if (length($resultado) < $soa) {
		$voa = "";
		$voa = $resultado;
		$soa = length($resultado);
	}
	
}

return $voa;

}

#Parte 3

sub ensamblar_avance_rapido {@secuencia = @_;

$longitud = scalar @secuencia-1;
%nueva;

#Como se pueden repetir secuencias, lo que hago es quedarme con las 
#secuencias únicas de esa lista.
foreach $s (0..$longitud) {
	$temporal = shift @secuencia;
	if (not (grep(/^$temporal/i, @secuencia))){
		push @secuencias,$temporal;
		} 
}


#creo todas las permutaciones posibles, por cada permutacion fusiono de 
#dos en dos todas ellas y guardo en un hash la cadena 1 y 2, separadas
#por un guión como clave y el valor será el del solapamiento.

my @actual = @secuencias;


while (@actual) { 
	@actual = permutar(@secuencias, @actual);
	$p = scalar @actual-1;
	foreach $i (0..$p) {
		%res = fusionar($actual[$i], $actual[$i+1]);
		$comb = $res{"cadena1"}."-".$res{"cadena2"};
		$nueva{$comb} = $res{'solapamiento'};
  }
  
}

#Ordeno el hash por su valor y guardo las claves en objetos_ordenados

@objetos_ordenados = sort { $nueva{$b} <=> $nueva{$a} } keys %nueva;
$len = scalar @objetos_ordenados;
@resultado = ();

#Saco de objetos_ordenados, el primer elemento que corresponderá al 
#valor más alto de solapamiento. Los separo y guardo las dos cadenas
#en $cad1 y $cad2. 

foreach $n (0..$len-1) {
	$new = shift @objetos_ordenados;
	@solucion = split ("-", $new);
	$cad1 = $solucion[0];
	$cad2 = $solucion[1];
	if ($cad1 ne $cad2) {
	#Si resultado está vacío, meto la cadena 1 y 2.
	if (scalar @resultado == 0) {
		push @resultado, $cad1;
		push @resultado, $cad2;
	#En cambio, si ya se ha incorporado, busco cadena 1 y 2 en @resultado,
	#y si se no se encuentra meto otra vez ambas cadenas.
	} else {
		if (not (grep(/^$cad1/i, @resultado) or grep(/^$cad2/i, @resultado))) {
			push @resultado, $cad1;
			push @resultado, $cad2;
		} 
		}
}
		}

$cant = scalar @resultado-1;

#Pueden que queden cadenas sin solapar entre ellas y que tampoco hayan
#sido la mejor solución de solapamiento anteriormente. Por ello,
#si la cadena de @secuencias no se encuentra en @resultado, se meterá

foreach $j (0..$longitud) {
	if (not (grep(/^$secuencias[$j]/i, @resultado))){
		push @resultado,$secuencias[$j];
		} 	
	}

#Por último, como @resultado, es un array ordenado por el mejor 
#solapamiento, fusiono dicha lista, obteniendo finalmente el resultado
#final.

$final = fusionar_lista(@resultado);
return $final;

}

sub ensamblar_avance_rapido2 {@secuencias = @_;
	
$len = scalar @secuencias-1;
my @numeros = 0..$len;
my $longitud = 2;
@actual = @secuencias;
my @actual = primera_variacion(@numeros, $longitud);
while (@actual) {
$cadena1 = $secuencias[$actual[0]];
$cadena2 = $secuencias[$actual[1]];
@actual = siguiente_variacion(@numeros, @actual);
if ($cadena1 ne $cadena2) {
$comb = $cadena1."-".$cadena2;
%resultado = fusionar($cadena1,$cadena2);
$nueva{$comb} = $resultado{'solapamiento'};
}
}


#Ordeno el hash por su valor y guardo las claves en objetos_ordenados

@objetos_ordenados = sort { $nueva{$b} <=> $nueva{$a} } keys %nueva;
$len = scalar @objetos_ordenados;
@resultado = ();

#Saco de objetos_ordenados, el primer elemento que corresponderá al 
#valor más alto de solapamiento. Los separo y guardo las dos cadenas
#en $cad1 y $cad2. 

foreach $n (0..$len-1) {
	$new = shift @objetos_ordenados;
	@solucion = split ("-", $new);
	$cad1 = $solucion[0];
	$cad2 = $solucion[1];
	if ($cad1 ne $cad2) {
	#Si resultado está vacío, meto la cadena 1 y 2.
	if (scalar @resultado == 0) {
		push @resultado, $cad1;
		push @resultado, $cad2;
	#En cambio, si ya se ha incorporado, busco cadena 1 y 2 en @resultado,
	#y si se no se encuentra meto otra vez ambas cadenas.
	} else {
		if (not (grep(/^$cad1/i, @resultado) or grep(/^$cad2/i, @resultado))) {
			push @resultado, $cad1;
			push @resultado, $cad2;
		} 
		}
}
		}

$cant = scalar @resultado-1;

#Pueden que queden cadenas sin solapar entre ellas y que tampoco hayan
#sido la mejor solución de solapamiento anteriormente. Por ello,
#si la cadena de @secuencias no se encuentra en @resultado, se meterá

foreach $j (0..$longitud) {
	if (not (grep(/^$secuencias[$j]/i, @resultado))){
		push @resultado,$secuencias[$j];
		} 	
	}

#Por último, como @resultado, es un array ordenado por el mejor 
#solapamiento, fusiono dicha lista, obteniendo finalmente el resultado
#final.
$final = fusionar_lista(@resultado);

return $final;
	
	
}

1;